[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)

# CKAD Exercises

Un groupe d'exercices qui peuvent vous aider à préparer pour la certification [Certified Kubernetes Application Developer](https://www.cncf.io/certification/ckad/), proposée par la **Cloud Native Computing Foundation**. Cette certification est subdivisée en plusieurs "domaines", qu'il faut être en capacité de couvrir pour réussir le mieux possibles les exercices à effectuer lors du passage de l'examen.

Durant cet examen, vous avez le droit de garder un unique onglet dans votre navigateur, ovuert sur la documentation officielle de kubernetes (kubernetes.io). Essayez donc du mieux possible d'avoir l'habitude de naviguer sur cette documentation, pour que vous puissiez rapidement trouver le contenu nécessaire. Evidemment, il est conseillé de lire les documents correspondants à la documentation officielle avant de chercher à effectuer les exercices présents dans ce dépôt GIT.

## Contents
- [L'examen de passage du CKAD](exam.md)
- [Core Concepts - 13%](a.core_concepts.md)
- [Multi-container pods - 10%](b.multi_container_pods.md)
- [Pod design - 20%](c.pod_design.md)
- [Configuration - 18%](d.configuration.md)
- [Observability - 18%](e.observability.md)
- [Services and networking - 13%](f.services.md)
- [State persistence - 8%](g.state.md)

### Puis-je effectuer une Pull Request pour suggérer un ajout, une modification, un exercice supplémentaire, ou une solution alternative ?
Bien sûr !! Sentez-vous libre d'effectuer des Pull Requests, autant que vous voulez, du moment que vous conservez le format existant.

### References
- [here](https://github.com/twajr/ckad-prep-notes) from twajr
- [here](https://www.reddit.com/r/kubernetes/comments/9uydc1/passed_the_ckad_special_thanks_to_the_linux/) from Blockchain0
- [here](https://medium.com/devopslinks/my-story-towards-cka-ckad-and-some-tips-daf495e711a9) from abdennour
- [here](https://medium.com/chotot-techblog/tips-tricks-to-pass-certified-kubernetes-application-developer-ckad-exam-67c9e1b32e6e) from Anh Dang
